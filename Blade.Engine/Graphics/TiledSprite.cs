﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Graphics
{
	public class TiledSprite : Sprite
	{
		public TiledSprite(Texture2D texture, float widht, float height)
			: this(texture, widht, height, new Rectangle(0, 0, texture.Width, texture.Height))
		{
		}

		public TiledSprite(Texture2D texture, float width, float height, Rectangle src)
			: base(texture)
		{
			TexturePosition = src;
			Width = width;
			Height = height;
		}
	}
}
