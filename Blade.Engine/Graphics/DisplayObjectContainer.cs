﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Graphics
{
	public class DisplayObjectContainer : DisplayObject
	{
		protected List<DisplayObject> Childs;

		//Constructors
		public DisplayObjectContainer()
			: this(0, 0, 0, 0)
		{
		}

		public DisplayObjectContainer(float width, float height)
			: this(0, 0, width, height)
		{
		}

		public DisplayObjectContainer(float x, float y, float width, float height)
			: base(x, y, width, height)
		{
			Childs = new List<DisplayObject>();
			//PassTransformationsToChilds = true;
			//PassButtonEventsToAllChilds = true;
		}

		public override void Update(float delta)
		{
			base.Update(delta);
			foreach (DisplayObject child in Childs.ToList())
			{
				child.Update(delta);
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			foreach (DisplayObject child in Childs.ToList())
			{
				if (child.Visible)
				{
					child.Draw(spriteBatch);
				}
			}
		}

		public virtual void AddChildAt(DisplayObject c, int index)
		{
			Debug.Assert(index >= 0 && index < ChildsCount());
			c.Parent = this;
			Childs.Insert(index, c);
		}

		public virtual void AddChild(DisplayObject c)
		{
			c.Parent = this;
			Childs.Add(c);
		}

		public virtual bool Contains(DisplayObject obj)
		{
			foreach (DisplayObject child in Childs)
			{
				if (child == obj)
					return true;

				var container = child as DisplayObjectContainer;
				if (container != null && container.Contains(obj))
					return true;
			}

			return false;
		}

		public DisplayObject GetChildAt(int index)
		{
			Debug.Assert(index >= 0 && index < ChildsCount());
			return Childs[index];
		}

		public int GetChildIndex(DisplayObject obj)
		{
			return Childs.IndexOf(obj);
		}

		public void RemoveChild(DisplayObject c)
		{
			bool childRemoved = Childs.Remove(c);
			if (childRemoved)
			{
				c.Parent = null;
			}
		}

		public void RemoveChildAt(int index)
		{
			RemoveChild(GetChildAt(index));
		}

		public void RemoveAllChilds()
		{
			foreach (DisplayObject child in Childs)
			{
				child.Parent = null;
			}
			Childs.Clear();
		}

		public virtual DisplayObject ReplaceChildAt(DisplayObject c, int index)
		{
			Debug.Assert(index >= 0 && index < ChildsCount());
			DisplayObject oldChild = Childs[index];
			oldChild.Parent = null;
			c.Parent = this;
			Childs[index] = c;
			return oldChild;
		}

		public void SetChildIndex(DisplayObject child, int index)
		{
			Debug.Assert(index >= 0 && index < ChildsCount());
			int oldIndex = GetChildIndex(child);
			Debug.Assert(oldIndex != -1);
			if (index > oldIndex)
			{
				for (int i = oldIndex; i < index; ++i)
				{
					Childs[i] = Childs[i + 1];
				}
			}
			else if (index < oldIndex)
			{
				for (int i = oldIndex; i > index; --i)
				{
					Childs[i] = Childs[i - 1];
				}
			}
			Childs[index] = child;
		}

		public void SwapChildren(DisplayObject child1, DisplayObject child2)
		{
			int index1 = GetChildIndex(child1);
			int index2 = GetChildIndex(child2);

			SwapChildrenAt(index1, index2);
		}

		public void SwapChildrenAt(int index1, int index2)
		{
			Debug.Assert(index1 >= 0 && index1 < ChildsCount());
			Debug.Assert(index2 >= 0 && index2 < ChildsCount());
			DisplayObject temp = Childs[index1];
			Childs[index1] = Childs[index2];
			Childs[index2] = temp;
		}

		public List<DisplayObject> GetChilds()
		{
			return Childs;
		}

		public int ChildsCount()
		{
			return Childs.Count;
		}

		private void ResizeToFitChilds(bool horizontally, bool vertically)
		{
			Debug.Assert(horizontally || vertically);
			if (ChildsCount() == 0)
			{
				Width = Height = 0;
			}
			else
			{
				DisplayObject firtChild = GetChildAt(0);
				float left = firtChild.X;
				float top = firtChild.Y;
				float right = left + firtChild.Width;
				float bottom = top + firtChild.Height;
				for (int i = 1; i < ChildsCount(); i++)
				{
					DisplayObject child = GetChildAt(i);
					left = Math.Min(left, child.X);
					top = Math.Min(top, child.Y);
					right = Math.Max(right, child.X + child.Width);
					bottom = Math.Max(bottom, child.Y + child.Height);
				}
				foreach (DisplayObject child in Childs)
				{
					child.X = horizontally ? child.X - left : child.X;
					child.Y = vertically ? child.Y - top : child.Y;
				}
				Width = horizontally ? right - left : Width;
				Height = vertically ? bottom - top : Height;
			}
		}

		public void ResizeHorizontallyToFitChilds()
		{
			ResizeToFitChilds(true, false);
		}

		public void ResizeVerticallyToFitChilds()
		{
			ResizeToFitChilds(false, true);
		}

		public void ResizeToFitChilds()
		{
			ResizeToFitChilds(true, true);
		}
	}
}
