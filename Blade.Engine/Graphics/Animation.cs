﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blade.Engine.Graphics
{
	//TODO: Add support for multi-row animation
	public class Animation
	{
		public Texture2D Texture { get; set; }
		public Rectangle TextureRectangle;
		public int Rows { get; set; }
		public int Columns { get; set; }
		public int Ticks { get; set; }
		public int CurrentFrame { get; set; }
		public int TotalFrames { get; set; }
		public int AnimationFrames { get; set; }
		public bool IsLooped { get; set; }
        public bool IsPlaying { get; protected set; } = false;
        public Action OnCurrentAnimationEnd { get; set; }

        public Animation(Texture2D texture, int totalFrames, int animationFrames, int rows, int startingRow = 0, bool isLooped = false)
		{
			Texture = texture;
			Ticks = 0;
			TotalFrames = totalFrames;
			AnimationFrames = animationFrames;
			Rows = rows;
			CurrentFrame = 0;
			IsLooped = isLooped;
			TextureRectangle = new Rectangle(0, (Texture.Height / Rows) * startingRow, Texture.Width / TotalFrames, Texture.Height / Rows);
		}

		public void Play()
		{
			IsPlaying = true;
		}

		public void Reset()
		{
			CurrentFrame = 0;
			TextureRectangle.X = CurrentFrame * TextureRectangle.Width;
		}

		public void Update(float delta)
		{
			if (IsPlaying)
			{
				Ticks++;
				if (Ticks >= AnimationFrames)
				{
					Ticks = 0;
					if (CurrentFrame++ == TotalFrames - 1)
					{
						CurrentFrame = 0;
						if (!IsLooped)
						{
							IsPlaying = false;
                            if (OnCurrentAnimationEnd != null)
                            {
                                OnCurrentAnimationEnd();
                                OnCurrentAnimationEnd = null;
                            }
						}
					}
					TextureRectangle.X = CurrentFrame * TextureRectangle.Width;
					//TextureRectangle.Y = 0;
				}
			}
		}
	}
}
