﻿using System;
using Microsoft.Xna.Framework;

namespace Blade.Engine.Graphics.Shapes
{
    public class Circle
    {
        private Vector2 v;
        private Vector2 direction;
        private float distanceSquared;

        /// <summary> 
        /// Center position of the circle. 
        /// </summary> 
        public Vector2 Center;

        /// <summary> 
        /// Radius of the circle. 
        /// </summary> 
        public float Radius;

        /// <summary> 
        /// Size of the circle. 
        /// </summary> 
        public Vector2 Size;

        /// <summary> 
        /// Constructs a new circle. 
        /// </summary> 
        public Circle(Rectangle position)
        {
            this.distanceSquared = 0f;
            this.direction = Vector2.Zero;
            this.v = Vector2.Zero;
            this.Center = new Vector2(position.X + (position.Width / 2), position.Y + (position.Height / 2));
            this.Radius = position.Width / 2;

            //Size should be Radius * 2??
            Size.X = Center.X + Radius;
            Size.Y = Center.Y + Radius;
        }

        /// <summary> 
        /// Determines if a circle intersects a rectangle. 
        /// </summary> 
        /// <returns>True if the circle and rectangle overlap. False otherwise.</returns> 
        public bool Intersects(Rectangle rectangle)
        {
            this.v = new Vector2(MathHelper.Clamp(Center.X, rectangle.Left, rectangle.Right),
                                    MathHelper.Clamp(Center.Y, rectangle.Top, rectangle.Bottom));

            this.direction = Center - v;
            this.distanceSquared = direction.LengthSquared();

            return ((distanceSquared > 0) && (distanceSquared < Radius * Radius));
        }

        public bool Collides(Circle circle)
        {
            Vector2 Distance = Size - circle.Size;
            if (Distance.Length() < Radius + circle.Radius) // if the distance is less than the diameter
                return true;
            return false;
        }

        public bool ContainsPoint(Point point)
        {
            double distance = Math.Sqrt((point.X - Center.X) * (point.X - Center.X) +
                (point.Y - Center.Y) * (point.Y - Center.Y));
            if (distance < Radius)
                return true;
            return false;
        }
    }
}
