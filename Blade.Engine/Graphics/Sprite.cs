﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Graphics
{
	public class Sprite : DisplayObjectContainer
	{
		protected Texture2D _texture;

		public Texture2D Texture
		{
			get { return _texture; }
			set
			{
				_texture = value;
				if (_texture == null)
				{
					Width = Height = 0;
				}
				else
				{
					Width = _texture.Width;
					Height = _texture.Height;
				}
			}
		}

		protected Rectangle TexturePosition;

		public Sprite() { }

		public Sprite(Texture2D texture)
		{
			Texture = texture;
			TexturePosition = new Rectangle();
			TexturePosition.Width = _texture.Width;
			TexturePosition.Height = _texture.Height;
		}

		//TODO: Support Layer
		public override void Draw(SpriteBatch spriteBatch)
		{
			if (Visible)
			{
				for (int i = 0; i < Effects.Count; i++)
				{

				}
				spriteBatch.Draw(Texture, Position, TexturePosition, Color * _alpha, Rotation, Vector2.Zero, Scale, SpriteEffects, 0f);
			}
		}
	}
}
