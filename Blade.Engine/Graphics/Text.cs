﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Graphics
{
	public enum TextAlign
	{
		LEFT, CENTER, RIGHT
	}

	public struct FormattedString
	{
		public String text;
		public float width;

		public FormattedString(String text, float width)
		{
			this.text = text;
			this.width = width;
		}
	}

	public class Text : DisplayObjectContainer
	{
		private const float UNDEFINED_WIDTH = float.PositiveInfinity;

		//FIXME
		private static readonly float charOffset = 0;
		public float BorderSize = 2;

		private string _text;
		public string TextValue
		{
			get
			{
				return _text;
			}
			set
			{
				_text = value;
				SetText(_text, Width, false);
			}
		}

		public SpriteFont Font { get; set; }

		private List<FormattedString> lines;

		private TextAlign textAlign = TextAlign.LEFT;

		public Text(SpriteFont font, string text) : this(font, text, UNDEFINED_WIDTH)
		{
		}

		public Text(SpriteFont font, string text, float width)
		{
			Debug.Assert(text != null);
			Font = font;
			SetText(text, width, true);
		}

		public Text(SpriteFont font, string text, float width, float height)
		{
			Debug.Assert(text != null);
			Width = width;
			Height = height;
			Font = font;
			SetText(text, width, false);
		}

		public void SetText(String text)
		{
			if (_text != text)
			{
				SetText(text, Width, false);
			}
		}

		public void SetText(String text, float width, bool resize)
		{
			Debug.Assert(text != null);
			_text = text;
			lines = WrapString(text, width);
			if (resize)
			{
				if (width == UNDEFINED_WIDTH)
				{
					width = 0;
					foreach (FormattedString line in lines)
					{
						if (width < line.width)
							width = line.width;
					}
				}
				Width = width;
				//SetLineOffset(0);
			}
		}

		public List<FormattedString> WrapString(string text, float width)
		{
			List<FormattedString> formattedStrings = new List<FormattedString>();

			int strLen = text.Length;
			int xc = 0;
			float wordWidth = 0;
			int strStartIndex = 0;
			int wordLastCharIndex = 0;
			float stringWidth = 0;
			int charIndex = 0;

			while (charIndex < strLen)
			{
				int curCharIndex = charIndex;
				char curChar = text[curCharIndex];
				charIndex++;
				if ((curChar == ' ') || (curChar == '\n'))
				{
					wordLastCharIndex = curCharIndex;
					if ((stringWidth == 0) && (wordWidth > 0))
						wordWidth -= charOffset;

					stringWidth += wordWidth;
					wordWidth = 0;
					xc = charIndex;
					if (curChar == ' ')
					{
						xc--;
						wordWidth = GetCharWidth(curChar) + charOffset;
					}
				}
				else
				{
					wordWidth += GetCharWidth(curChar) + charOffset;
				}
				if ((((stringWidth + wordWidth) > width) && (wordLastCharIndex != strStartIndex)) || (curChar == '\n'))
				{
					string sub = text.Substring(strStartIndex, wordLastCharIndex - strStartIndex);
					formattedStrings.Add(new FormattedString(sub, Font.MeasureString(sub).X));

					char xcc;
					while (xc < text.Length && (xcc = text[xc]) == ' ')
					{
						wordWidth -= GetCharWidth(xcc) + charOffset;
						xc++;
					}
					wordWidth -= charOffset;
					strStartIndex = xc;
					wordLastCharIndex = strStartIndex;
					stringWidth = 0;
				}
			}
			if (wordWidth != 0)
			{
				string sub = text.Substring(strStartIndex, strLen - strStartIndex);
				formattedStrings.Add(new FormattedString(sub, Font.MeasureString(sub).X));
			}
			return formattedStrings;
		}

		private int GetCharWidth(char ch)
		{
			return (int)Font.MeasureString(ch.ToString()).X;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			float y = 0;
			foreach (var line in lines)
			{
				//TODO: FIX
				float x = 0;
				if (textAlign == TextAlign.CENTER)
				{
					x = 0.5f * (Width - line.width);
				}
				else if (textAlign == TextAlign.RIGHT)
				{
					x = Width - line.width;
				}

				if (DrawBorder)
				{
					spriteBatch.DrawString(Font, line.text, new Vector2(Position.X - BorderSize, Position.Y + y), BorderColor * Alpha, Rotation, Vector2.Zero, Scale, SpriteEffects, 0f);
					spriteBatch.DrawString(Font, line.text, new Vector2(Position.X + BorderSize, Position.Y + y), BorderColor * Alpha, Rotation, Vector2.Zero, Scale, SpriteEffects, 0f);
					spriteBatch.DrawString(Font, line.text, new Vector2(Position.X, Position.Y + y - BorderSize), BorderColor * Alpha, Rotation, Vector2.Zero, Scale, SpriteEffects, 0f);
					spriteBatch.DrawString(Font, line.text, new Vector2(Position.X, Position.Y + y + BorderSize), BorderColor * Alpha, Rotation, Vector2.Zero, Scale, SpriteEffects, 0f);
				}
				spriteBatch.DrawString(Font, line.text, new Vector2(Position.X, Position.Y + y), Color * Alpha, Rotation, Vector2.Zero, Scale, SpriteEffects, 0f);
				y += Font.LineSpacing;
			}
			
			base.Draw(spriteBatch);
		}
	}
}
