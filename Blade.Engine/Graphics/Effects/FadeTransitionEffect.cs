﻿using Blade.Engine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Graphics.Effects
{
    public class FadeTransitionEffect : TransitionEffect
    {
        public byte FadingIndex { get; set; }
        public byte AnimationTime { get; set; }
        public float AlphaVariation { get; set; }
        protected float fadeOpacity = 0;

        public FadeTransitionEffect(DisplayObject CurrentObject, DisplayObject NextObject, byte frames = 120) :base(CurrentObject, NextObject)
        {
            FadingIndex = AnimationTime = frames;
            CurrentObject.Alpha = 1f;
            NextObject.Alpha = 0;
            AlphaVariation = (1f / FadingIndex) * 2;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            CurrentObject.Draw(spriteBatch);
            spriteBatch.Draw(ResourceManager.BLANK_SQUARE, Vector2.Zero, null, Color.Black * fadeOpacity, 0f, Vector2.Zero, new Vector2(960, 540), SpriteEffects.None, 0f);
            NextObject.Draw(spriteBatch);
        }

        public override void Update(float delta)
        {
            if (FadingIndex >= (AnimationTime / 2))
            {
                FadingIndex--;
                CurrentObject.Alpha -= AlphaVariation;
                fadeOpacity += AlphaVariation;
            }
            else if (FadingIndex < (AnimationTime / 2) && FadingIndex > 0)
            {
                FadingIndex--;
                NextObject.Alpha += AlphaVariation;
                fadeOpacity -= AlphaVariation;
            }
            else if (FadingIndex == 0)
            {
                CurrentObject.Alpha = 0;
                NextObject.Alpha = 1;
                IsFinished = true;
                if (OnTransitionEnded != null)
                {
                    OnTransitionEnded();
                }
            }
        }
    }
}
