﻿using System;
using Microsoft.Xna.Framework;

namespace Blade.Engine.Graphics.Effects
{
	class TestCallback
	{
		public int arg1;
		public string arg2;
	}

	class Sender
	{
		private Action<TestCallback> Callback;
		public Sender(Action<TestCallback> callback)
		{
			Callback = callback;
		}

		private void OnEvent()
		{
			Callback(new TestCallback
			{
				arg1 = 24,
				arg2 = "adwdw"
			});
		}
	}

	public class FadeInEffect : DisplayEffect
	{
		public byte FadingIndex { get; set; }
		public float AddToAlpha { get; set; }

		public FadeInEffect(DisplayObject owner, byte frames) : base(owner)
		{
			FadingIndex = frames;
			Owner.Alpha = 0;
			Owner.Color = new Color(new Vector3(1, 1, 1));
			AddToAlpha = 1f / FadingIndex;
		}

		private Action<DisplayEffect> Callback;
		public FadeInEffect(DisplayObject owner, byte frames, Action<DisplayEffect> callBack)
			: this(owner, frames)
		{
			Callback = callBack;
		}

		public override void Update(float delta)
		{
			if (FadingIndex > 0)
			{
				FadingIndex--;
				Owner.Alpha += AddToAlpha;
			}
			if (FadingIndex == 0)
			{
				IsFinished = true;
				if (Callback != null)
				{
					Callback(this);
				}
			}
		}
	}
}
