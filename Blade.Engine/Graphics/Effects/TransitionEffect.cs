﻿using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blade.Engine.Graphics.Effects
{
    public abstract class TransitionEffect
    {
        public DisplayObject CurrentObject { get; set; }
        public DisplayObject NextObject { get; set; }
        public bool IsFinished { get; protected set; }
        public Action OnFirstScreenEnded { get; set; }
        public Action OnTransitionEnded { get; set; }

        protected TransitionEffect(DisplayObject CurrentObject, DisplayObject NextObject)
        {
            this.CurrentObject = CurrentObject;
            this.NextObject = NextObject;
        }

        public abstract void Draw(SpriteBatch spriteBatch);        

		public abstract void Update(float delta);
    }
}
