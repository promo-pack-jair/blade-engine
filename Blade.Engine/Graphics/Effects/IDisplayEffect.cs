﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blade.Engine.Graphics.Effects
{
    interface IDisplayEffect
    {
        DisplayObject Owner { get; set; }
        bool IsFinished { get; set; }
        void Update(float delta);
    }
}
