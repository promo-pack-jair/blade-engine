﻿namespace Blade.Engine.Graphics.Effects
{
	public abstract class DisplayEffect
	{
		public DisplayObject Owner { get; set; }
		public bool IsFinished { get; protected set; }

		protected DisplayEffect(DisplayObject owner)
		{
			Owner = owner;
		}

		public abstract void Update(float delta);
	}
}
