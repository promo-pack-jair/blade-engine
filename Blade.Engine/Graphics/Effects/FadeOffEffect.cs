﻿namespace Blade.Engine.Graphics.Effects
{
	public class FadeOffEffect : DisplayEffect
	{
		public byte FadingIndex { get; set; }
		public float SubstractFromAlpha { get; set; }

		public FadeOffEffect(DisplayObject owner, byte frames)
			: base(owner)
		{
			FadingIndex = frames;
            Owner.Alpha = 1f;
            SubstractFromAlpha = 1f / FadingIndex;
		}

		public override void Update(float delta)
		{
			if (FadingIndex > 0)
			{
				FadingIndex--;
				Owner.Alpha -= SubstractFromAlpha;
			}
            else if (FadingIndex == 0)
			{
				Owner.Alpha = 0;
				IsFinished = true;
			}
		}
	}
}
