﻿using Blade.Engine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blade.Engine.Graphics.Effects
{
    public class FadeWhiteTransitionEffect : FadeTransitionEffect
    {
        public FadeWhiteTransitionEffect(DisplayObject CurrentObject, DisplayObject NextObject, byte frames = 120) :base(CurrentObject, NextObject, frames)
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            CurrentObject.Draw(spriteBatch);
            spriteBatch.Draw(ResourceManager.BLANK_SQUARE, Vector2.Zero, null, Color.White * fadeOpacity, 0f, Vector2.Zero, new Vector2(960, 540), SpriteEffects.None, 0f);
            NextObject.Draw(spriteBatch);
        }
    }
}
