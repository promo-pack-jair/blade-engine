﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Graphics
{
    public class AnimatedSprite : DisplayObject
	{
		public Dictionary<string, Animation> Animations { get; private set; } = new Dictionary<string, Animation>();
		public Animation PreviousAnimation { get; set; }
		public Animation CurrentAnimation { get; set; }
        private Animation _NextAnimation;
        public Animation NextAnimation {
            get
            {
                return _NextAnimation;
            }
            set
            {
                _NextAnimation = value;
                CurrentAnimation.OnCurrentAnimationEnd = () =>
                {
                    ChangeCurrentAnimation(_NextAnimation, true);
                    _NextAnimation = null;
                };
            }
        }

        public void AddAnimation(string name, Animation animation)
		{
			Animations.Add(name, animation);
        }

		public void AddDefaultAnimation(string name, Animation animation, bool startPlaying = true)
		{
			CurrentAnimation = animation;
			Animations.Add(name, animation);
            if (startPlaying)
            {
                CurrentAnimation.Play();
            }
        }

		public void ChangeCurrentAnimation(string name, bool startPlaying = true)
		{
            ChangeCurrentAnimation(Animations[name], startPlaying);
        }

        public void ChangeCurrentAnimation(Animation animation, bool startPlaying = true)
        {
            PreviousAnimation = CurrentAnimation;
            CurrentAnimation = animation;
            CurrentAnimation.Ticks = 0;
            if (startPlaying)
            {
                CurrentAnimation.Play();
            }
        }

        public override void Update(float delta)
		{
			base.Update(delta);
			CurrentAnimation.Update(delta);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (Visible)
			{
				for (int i = 0; i < Effects.Count; i++)
				{

				}
				//FIXME: If is faster than ternary (http://stackoverflow.com/questions/17328641/ternary-operator-is-twice-as-slow-as-an-if-else-block)
				spriteBatch.Draw(CurrentAnimation.Texture, Position, CurrentAnimation.TextureRectangle,
					Color * Alpha, Rotation, (Parent != null) ? Parent.Position : Vector2.Zero, Scale, SpriteEffects, 0f);
			}
		}
	}
}
