﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blade.Engine.Graphics.Effects;

namespace Blade.Engine.Graphics
{
	//TODO: Add Contains point (click?)
	//TODO: Add OnClickListener
	//TODO: Add Collision
	public abstract class DisplayObject
	{
		public const float ALIGN_MIN = 0.0f;
		public const float ALIGN_CENTER = 0.5f;
		public const float ALIGN_MAX = 1.0f;

		protected float _alpha = 1f;
		public float Alpha
		{
			get
            {
                if (Parent != null)
                {
                    return Parent.Alpha * _alpha;
                }
                return _alpha;
            }
			set
			{
				if (value > 1f || value < 0f) return;
				_alpha = value;
			}
		}
		public Color Color { get; set; } = Color.White;
        
		//TODO: Add Origin Vector, default Vector2.Zero, onParent added change to parent.Position
		public Vector2 Position = Vector2.Zero;
		public float X
		{
			get { return Position.X; }
			set { Position.X = value; }
		}
		public float Y
		{
			get { return Position.Y; }
			set { Position.Y = value; }
		}
        private float _width = 0;
		public float Width { get { return _width * ScaleX; }  set { _width = value; } }
        private float _height = 0;
        public float Height { get { return _height * ScaleY; } set { _height = value; } }
		public float Rotation { get; set; }
		public bool Visible { get; set; }

		protected DisplayObject()
            : this(0, 0)
        {
        }

        protected DisplayObject(float width, float height)
            : this(0, 0, width, height)
        {
        }

		protected DisplayObject(float x, float y, float width, float height)
		{
			X = x;
			Y = y;
			Width = width;
			Height = height;
			Visible = true;
		}

		//Scale
		protected Vector2 Scale = new Vector2(1, 1);
		public float ScaleX
		{
			get { return Scale.X; }
			set { Scale.X = value; }
		}
		public float ScaleY
		{
			get { return Scale.Y; }
			set { Scale.Y = value; }
		}

		public SpriteEffects SpriteEffects { get; set; }

		public virtual void Update(float delta)
		{
			for (int i = 0; i < Effects.Count; i++)
			{
				if (!Effects[i].IsFinished)
				{
					Effects[i].Update(delta);
				}
				else
				{
					Effects.RemoveAt(i);
					i--;
				}
			}
		}

		public DisplayObjectContainer Parent { get; set; } 

		//TODO: Support Layer
		public abstract void Draw(SpriteBatch spriteBatch);

		//For parent Alignment
		private float drawX;
		private float drawY;

		public float AlignX { get; set; }
		public float AlignY { get; set; }

		public float ParentAlignX { get; set; }
		public float ParentAlignY { get; set; }

#if DEBUG
		public Color BorderColor = Color.White;
		public bool DrawBorder;
#endif

		public virtual void PreDraw()
		{
			// align to parent
			drawX = X - Width * AlignX;
			drawY = Y - Height * AlignY;

			if (Parent != null)
			{
				drawX += Parent.Width * ParentAlignX;
				drawY += Parent.Height * ParentAlignY;
			}
		}

		//Display Effects
		public List<DisplayEffect> Effects = new List<DisplayEffect>();
	}
}
