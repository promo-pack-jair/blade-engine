﻿namespace Blade.Engine.ContentImporters
{
	public class TileMapInfo
	{
		public int MapWidth { get; set; }
		public int MapHeigth { get; set; }
		public int[] Tiles { get; set; }
		public string Name { get; set; }

		public TileMapInfo(JSONNode jsonObject)
		{
			Name = jsonObject["Name"];
			MapWidth = jsonObject["MapWidth"].AsInt;
			MapHeigth = jsonObject["MapHeigth"].AsInt;
			Tiles = new int[MapWidth * MapHeigth];
			var tiles = jsonObject["Tiles"].AsArray;

			for (int i = 0; i < Tiles.Length; i++)
			{
				//TODO: Change Tile structure definition, use short and narrower types for isSolid and tileSheetID
				Tiles[i] = tiles[i].AsInt;
				//The first byte indicates if its Solid
				bool isSolid = (Tiles[i] >> 24) != 0;
				//The second byte indicates the Id of the Tilesheet
				int tileSheetId = Tiles[i] >> 16;
				//The last two bytes represent the position of the tile in coordinates based on a tile Maximum Width
				int tilePosition = Tiles[i] >> 8;
			}
		}
	}
}
