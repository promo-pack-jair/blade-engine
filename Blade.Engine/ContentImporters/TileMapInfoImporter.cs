﻿using System.IO;

namespace Blade.Engine.ContentImporters
{
	internal sealed class TileMapInfoImporter : CustomContentImporter<TileMapInfo>
	{
		//TODO: Optimize unnecessary conversions
		public override object LoadResource(string resourceName)
		{
			string jsonString = "";
			using (var reader = new StreamReader(resourceName + ".tilemap"))
			{
				jsonString = reader.ReadToEnd();
				reader.Close();
			}
			return new TileMapInfo(JSONNode.Parse(jsonString));
		}
	}
}
