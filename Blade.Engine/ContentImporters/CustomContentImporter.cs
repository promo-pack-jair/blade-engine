﻿namespace Blade.Engine.ContentImporters
{
	public interface ICustomContentImporter
	{
		object LoadResource(string resourceName);
	}

	public abstract class CustomContentImporter<T> : ICustomContentImporter
	{
		public abstract object LoadResource(string resourceName);
		public virtual void UnloadResource(string resourceName) { }
	}
}
