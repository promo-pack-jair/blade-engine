﻿using System;
using Microsoft.Xna.Framework.Content;

namespace Blade.Engine.ContentImporters.JSON
{
    public class JsonReader : ContentTypeReader<JSONNode>
    {
        protected override JSONNode Read(ContentReader reader, JSONNode existingInstance)
        {
            reader.ReadString();
            /*
            var textureAssetCount = input.ReadInt32();
            var assets = new List<string>();

            for (var i = 0; i < textureAssetCount; i++)
            {
                var assetName = input.ReadString();
                assets.Add(assetName);
            }

            var json = input.ReadString();
            var fontFile = JsonConvert.DeserializeObject<FontFile>(json);
            var texture = input.ContentManager.Load<Texture2D>(assets.First());
            */
            return JSONNode.Deserialize(reader);
        }
    }
}
