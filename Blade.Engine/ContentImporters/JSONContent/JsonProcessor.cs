﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace Blade.Engine.ContentImporters.JSON
{
    [ContentProcessor(DisplayName = "Json Processor - Blade.Engine.ContentImporters")]
    public class JsonProcessor : ContentProcessor<JsonFile, JsonProcessorResult>
    {
        public override JsonProcessorResult Process(JsonFile input, ContentProcessorContext context)
        {
            try
            {
                context.Logger.LogMessage("Processing Json");
                // JsonConvert.SerializeObject(input);

                /*
                foreach (var fontPage in json.Pages)
                {
                    var assetName = Path.GetFileNameWithoutExtension(fontPage.File);
                    output.TextureAssets.Add(assetName);
                }
                */

                return new JsonProcessorResult(input, context.Logger);

            }
            catch (Exception ex)
            {
                context.Logger.LogMessage("Error {0}", ex);
                throw;
            }

        }
    }
}
