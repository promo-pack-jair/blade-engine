﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blade.Engine.ContentImporters.JSON
{
    public class JsonFile
    {
        public string Contents { get; private set; }

        public JsonFile(string Contents)
        {
            Contents = Contents;
        }
    }
}
