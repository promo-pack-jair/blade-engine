﻿using Microsoft.Xna.Framework.Content.Pipeline;

namespace Blade.Engine.ContentImporters.JSON
{
    public class JsonProcessorResult
    {
        public JsonFile File { get; private set; }
        public ContentBuildLogger Logger { get; private set; }

        public JsonProcessorResult(JsonFile file, ContentBuildLogger logger)
        {
            File = file;
            Logger = logger;
        }
    }
}
