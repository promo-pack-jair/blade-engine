﻿using Microsoft.Xna.Framework.Content.Pipeline;
using System;
using System.IO;

namespace Blade.Engine.ContentImporters.JSON
{
    [ContentImporter(".json", ".bin", DefaultProcessor = "BitmapFontProcessor",
    DisplayName = "Json Importer - Blade.Engine")]
    public class JsonImporter : ContentImporter<JsonFile>
    {
        public override JsonFile Import(string filename, ContentImporterContext context)
        {
            if (filename == null) throw new ArgumentNullException(nameof(filename));
            context.Logger.LogMessage("Importing JSON file: {0}", filename);
            
            using (var reader = new StreamReader(filename))
            {
                var jsonString = reader.ReadToEnd();
                reader.Close();
                return new JsonFile(jsonString);
            }
        }
    }
}
