﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Blade.Engine.ContentImporters.JSON
{
    public class JsonWriter : ContentTypeWriter<JsonProcessorResult>
    {
        protected override void Write(ContentWriter writer, JsonProcessorResult value)
        {
            writer.Write(value.File.Contents);
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(JSONNode).AssemblyQualifiedName;
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return  typeof(JsonReader).AssemblyQualifiedName;
        }
    }
}
