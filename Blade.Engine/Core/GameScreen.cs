﻿using Blade.Engine.Graphics;

namespace Blade.Engine.Core
{
	public abstract class GameScreen : DisplayObjectContainer
	{
		public bool IsPopup { get; protected set; }
		public GameModule ContainingModule { get; set; }

        public virtual void OnAttach() { }
        public virtual void OnResume() { }
        public virtual void OnPause() { }

        public virtual void Destroy()
		{
			while (ChildsCount() > 0)
			{
				RemoveChildAt(0);
			}
		}
	}
}
