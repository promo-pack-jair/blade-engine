﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Blade.Engine.Core
{
    //TODO: Add support for looping through segment of track
    public static class AudioManager
	{
		private static List<SoundEffectInstance> Sounds = new List<SoundEffectInstance>();

		private static float MasterVolume, EffectsVolume, MusicVolume, VoiceVolume = 1f;

		private static bool IsMute;

		public static void StopAll()
		{
			MediaPlayer.Stop();
		}

		public static void ChangeBGMVolume(float volume)
		{
			volume = MathHelper.Clamp(volume, 0, 1);
			MediaPlayer.Volume = volume;
		}

		public static MediaState GetBGMState()
		{
			return MediaPlayer.State;
		}

		public static void PlayBGM(string resourceName, bool loop = true)
        {
			MediaPlayer.Play(ResourceManager.LoadSong(resourceName));
			MediaPlayer.IsRepeating = loop;
        }

		public static void PauseBGM()
        {
			MediaPlayer.Pause();
        }

		public static void ResumeBGM()
		{
			MediaPlayer.Resume();
		}

		public static void StopBGM()
		{
			MediaPlayer.Stop();
        }

		//TODO: Keep a list of Active Sounds to manipulate

		// Load
		static void Load(string resourceName)
		{
			// Load the Sound
			SoundEffect sound = ResourceManager.Get<SoundEffect>(resourceName);
			// Create an instance to play the sound
			var soundInstance = sound.CreateInstance();
			Sounds.Add(soundInstance);
		}

        //Use channels
		public static void PlaySE(string resourceName, float volume = 1.0f, float pan = 0.0f, float pitch = 0.0f, bool looped = false)
        {
			SoundEffectInstance sfx = ResourceManager.LoadSoundEffect(resourceName).CreateInstance();
			sfx.Volume = volume;
			sfx.Pan = pan;
			sfx.Pitch = pitch;
			sfx.IsLooped = looped;
			sfx.Play();
        }

		public static void StopSE(string resourceName)
        {
			SoundEffectInstance sfx = ResourceManager.LoadSoundEffect(resourceName).CreateInstance();
			sfx.Stop();
			sfx.Dispose();
        }

		public static SoundState GetSEState(string resourceName)
		{
			return ResourceManager.LoadSoundEffect(resourceName).CreateInstance().State;
		}
    }
}
