﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Blade.Engine.Core
{
	//TODO: Support for OnKeyLongPress
	//TODO: Support Multi-Touch
	//TODO: Support subscribe to OnClicked Event (For when a specific sprite is touched/clicked)
	//TODO: Support Double-Click
	//TODO: Support for Thumbsticks
	//TODO: Block input for some time (frames)?
    public static class GameInput
    {
		//TODO: Method to assign a GamePad from input?
		private static Buttons[] GAMEPAD_BUTTONS = 
        {
            Buttons.DPadUp,
            Buttons.DPadDown,
            Buttons.DPadLeft,
            Buttons.DPadRight,
            Buttons.Start,
            Buttons.Back,
            Buttons.LeftStick,
            Buttons.RightStick,
            Buttons.LeftShoulder,
            Buttons.RightShoulder,
            Buttons.BigButton,
            Buttons.A,
            Buttons.B,
            Buttons.X,
            Buttons.Y,
            Buttons.LeftThumbstickLeft,
            Buttons.RightTrigger,
            Buttons.LeftTrigger,
            Buttons.RightThumbstickUp,
            Buttons.RightThumbstickDown,
            Buttons.RightThumbstickRight,
            Buttons.RightThumbstickLeft,
            Buttons.LeftThumbstickUp,
            Buttons.LeftThumbstickDown,
            Buttons.LeftThumbstickRight
        };

		private static bool IsInitialized;

		public static void Initialize(int gamepadPlayers)
		{
			if (!IsInitialized)
			{
				GamePadStates = new GamePadState[gamepadPlayers];
				IsInitialized = true;
			}
		}

		public static void Update()
		{
			//if (CurrentGameMode == GameMode.Gamepad)
			UpdateGamePad();
#if WINDOWS || LINUX || MACOS
            UpdateKeyboard();
#endif
#if WINDOWS || WINDOWSPHONE || ANDROID || IOS
            UpdatePointer();
#endif
		}

        //---------------------------------------------------------------------------------
        //	GamePad
        //---------------------------------------------------------------------------------

        private static List<GamePadListener> GamePadListeners = new List<GamePadListener>();
        private static GamePadState[] GamePadStates;
        public static Buttons[] GamepadUsedButtons;

        //TODO: Listen for specific keys per Player?
        public static void ReinitializeGamePadKeys(int gamepadPlayers, Buttons[] gamepadUsedButtons)
		{
			GamePadStates = new GamePadState[gamepadPlayers];
			GamepadUsedButtons = gamepadUsedButtons;

			for (int playerIndex = 0; playerIndex < gamepadPlayers; ++playerIndex)
			{
				PlayerIndex player = (PlayerIndex) playerIndex;
				GamePadStates[playerIndex] = GamePad.GetState(player);
			}
		}

		private static void UpdateGamePad()
		{
			if (GamePadListeners.Count > 0)
			{
				for (int playerIndex = 0; playerIndex < GamePadStates.Length; ++playerIndex)
				{
					UpdateGamePad(playerIndex);
				}
			}
		}

		private static void UpdateGamePad(int playerIndex)
		{
			GamePadState oldState = GamePadStates[playerIndex];
			GamePadStates[playerIndex] = GamePad.GetState((PlayerIndex)playerIndex);

			if (GamePadStates[playerIndex].IsConnected && !oldState.IsConnected)
			{
				foreach (GamePadListener listener in GamePadListeners)
				{
					listener.GamePadDisconnected(playerIndex);
				}
			}
			else if (GamePadStates[playerIndex].IsConnected && oldState.IsConnected)
			{
				foreach (GamePadListener listener in GamePadListeners)
				{
					listener.GamePadConnected(playerIndex);
				}
			}

			foreach (Buttons button in GamepadUsedButtons)
			{
				if (GamePadStates[playerIndex].IsButtonDown(button) && oldState.IsButtonUp(button))
				{
					foreach (GamePadListener listener in GamePadListeners)
					{
						listener.ButtonPressed(new ButtonEventArg(playerIndex, button));
					}
				}
				if (GamePadStates[playerIndex].IsButtonUp(button) && oldState.IsButtonDown(button))
				{
					foreach (GamePadListener listener in GamePadListeners)
					{
						listener.ButtonReleased(new ButtonEventArg(playerIndex, button));
					}
				}
			}
		}

		public static void AddGamePadListener(GamePadListener listener)
		{
			GamePadListeners.Add(listener);
		}

		public static void RemoveGamePadListener(GamePadListener listener)
		{
			GamePadListeners.Remove(listener);
		}

		public static bool IsGamePadConnected(int playerIndex)
		{
			return GamePadStates[playerIndex].IsConnected;
		}

		public static int GetPlayersCount()
		{
			return GamePadStates.Length;
		}

		public static GamePadThumbSticks ThumbSticks(int playerIndex)
		{
			return GamePadStates[playerIndex].ThumbSticks;
		}

		public static GamePadTriggers Triggers()
		{
			return Triggers(0);
		}

		public static GamePadTriggers Triggers(int playerIndex)
		{
			return GamePadStates[playerIndex].Triggers;
		}

        //---------------------------------------------------------------------------------
        //	Keyboard
        //---------------------------------------------------------------------------------

        private static List<KeyboardListener> KeyboardListeners = new List<KeyboardListener>();
        private static Queue<KeyboardListener> KeyboardListenersToAdd = new Queue<KeyboardListener>();
        private static Queue<KeyboardListener> KeyboardListenersToRemove = new Queue<KeyboardListener>();
        private static KeyboardState KeyboardState;
        public static Keys[] KeyboardListeningKeys;

        private static void UpdateKeyboard()
		{
			KeyboardState oldState = KeyboardState;
			KeyboardState = Keyboard.GetState();

            for (int i = KeyboardListenersToAdd.Count; i > 0; i--)
            {
                KeyboardListeners.Add(KeyboardListenersToAdd.Dequeue());
            }
            for (int i = KeyboardListenersToRemove.Count; i > 0; i--)
            {
                KeyboardListeners.Remove(KeyboardListenersToRemove.Dequeue());
            }

            if (KeyboardListeners.Count > 0)
			{
				Keys[] oldKeys = oldState.GetPressedKeys();
				Keys[] newKeys = KeyboardState.GetPressedKeys().Intersect(KeyboardListeningKeys).ToArray();

				for (int i = 0; i < newKeys.Length; ++i)
				{
					if (!oldKeys.Contains(newKeys[i]))
					{
						foreach (KeyboardListener listener in KeyboardListeners)
						{
							listener.KeyPressed(newKeys[i]);
						}
					}
				}
				for (int i = 0; i < oldKeys.Length; ++i)
				{
					if (!newKeys.Contains(oldKeys[i]))
					{
						foreach (KeyboardListener listener in KeyboardListeners)
						{
							listener.KeyReleased(oldKeys[i]);
						}
					}
				}
			}
		}

		public static void AddKeyboardListener(KeyboardListener listener)
		{
            KeyboardListenersToAdd.Enqueue(listener);
            //KeyboardListeners.Add(listener);
		}

		public static void RemoveKeyboardListener(KeyboardListener listener)
		{
            KeyboardListenersToRemove.Enqueue(listener);
            //KeyboardListeners.Remove(listener);
		}

        //---------------------------------------------------------------------------------
        //	Touch
        //---------------------------------------------------------------------------------

        private static List<TouchListener> TouchListeners = new List<TouchListener>();
        private static bool PointerDown;
		private static int PointerLastX;
		private static int PointerLastY;

		private static void UpdatePointer()
		{
			if (PointerDown)
			{
				if (Mouse.GetState().LeftButton == ButtonState.Released)
				{
					PointerDown = false;
					PointerLastX = Mouse.GetState().X;
					PointerLastY = Mouse.GetState().Y;
					foreach (TouchListener listener in TouchListeners)
					{
						listener.PointerReleased(PointerLastX, PointerLastY);
					}
				}
				else
				{
					int pointerX = Mouse.GetState().X;
					int pointerY = Mouse.GetState().Y;
					if (pointerX != PointerLastX || pointerY != PointerLastY)
					{
						foreach (TouchListener listener in TouchListeners)
						{
							listener.PointerDragged(PointerLastX, PointerLastY);
						}
					}
					PointerLastX = pointerX;
					PointerLastY = pointerY;
				}
			}
			else
			{
				if (Mouse.GetState().LeftButton == ButtonState.Pressed)
				{
					PointerDown = true;
					PointerLastX = Mouse.GetState().X;
					PointerLastY = Mouse.GetState().Y;
					foreach (TouchListener listener in TouchListeners)
					{
						listener.PointerPressed(PointerLastX, PointerLastY);
					}
				}
			}
		}

		public static void AddTouchListener(TouchListener listener)
		{
			TouchListeners.Add(listener);
		}

		public static void RemoveTouchListener(TouchListener listener)
		{
			TouchListeners.Remove(listener);
		}
    }
}
