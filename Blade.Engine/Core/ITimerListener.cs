﻿using System;

namespace Blade.Engine.Core
{
	public class TimerEventArgs : EventArgs
	{
		public int Ticks;
		public float ElapsedTime;
		public object Tag;

		public TimerEventArgs(int ticks, float elapsedTime, object tag)
		{
			Ticks = ticks;
			ElapsedTime = elapsedTime;
			Tag = tag;
		}
		
	}

	public abstract class TimerEventSubscriptor
	{
		public bool IsFinished;
		public bool Repeat;
		protected int Ticks;
		protected float ElapsedTime;
		public ITimerManager TimerManager;
		public object Tag;
		public event EventHandler<TimerEventArgs> OnTimer;

		protected TimerEventSubscriptor(EventHandler<TimerEventArgs> onTimer, bool repeat, object tag)
		{
			Repeat = repeat;
			Tag = tag;
			OnTimer += onTimer;
			Ticks = 0;
			ElapsedTime = 0f;
		}

		public virtual void Update(float delta)
		{
			Ticks++;
			ElapsedTime += delta;
		}

		public void FireEvent()
		{
			if (OnTimer != null)
			{
				OnTimer(this, new TimerEventArgs(Ticks, ElapsedTime, Tag));
			}
			if (!Repeat)
			{
				TimerManager.RemoveListener(this);
				IsFinished = true;
			}
			else
			{
				Ticks = 0;
				ElapsedTime = 0f;
			}
		}
	}

	public class TickSubscriptor : TimerEventSubscriptor
	{
		public int MaxTicks;

		public TickSubscriptor(EventHandler<TimerEventArgs> onTimer, bool repeat, object tag, int targetTicks)
			: base(onTimer, repeat, tag)
		{
			MaxTicks = targetTicks;
		}

		public override void Update(float delta)
		{
			base.Update(delta);
			if (Ticks == MaxTicks)
			{
				FireEvent();
			}
		}
	}

	public class TimeSubscriptor : TimerEventSubscriptor
	{
		public float TargetTime;

		public TimeSubscriptor(EventHandler<TimerEventArgs> onTimer, bool repeat, object tag, float targetTime)
			: base(onTimer, repeat, tag)
		{
			TargetTime = targetTime;
		}

		public override void Update(float delta)
		{
			base.Update(delta);
			if (ElapsedTime >= TargetTime)
			{
				FireEvent();
			}
		}
	}

	public interface ITimerManager
	{
		void AddListener(TimerEventSubscriptor listener);
		void RemoveListener(TimerEventSubscriptor listener);
	}
}