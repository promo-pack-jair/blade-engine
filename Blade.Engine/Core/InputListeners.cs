﻿using Microsoft.Xna.Framework.Input;

namespace Blade.Engine.Core
{
	public struct ButtonEventArg
	{
		public Buttons Button;
		public int PlayerIndex;

		public ButtonEventArg(int playerIndex, Buttons button)
		{
			PlayerIndex = playerIndex;
			Button = button;
		}
	}

	public interface KeyboardListener
	{
		void KeyPressed(Keys key);
		void KeyReleased(Keys key);
	}

	public interface GamePadListener
	{
		void ButtonPressed(ButtonEventArg e);
		void ButtonReleased(ButtonEventArg e);
		void GamePadConnected(int playerIndex);
		void GamePadDisconnected(int playerIndex);
	}

	public interface TouchListener
	{
		void PointerPressed(int x, int y);
		void PointerDragged(int x, int y);
		void PointerReleased(int x, int y);
	}

	public abstract class GameButton
	{
		public abstract Buttons ToButton();
		public abstract Keys ToKey();
	}
}
