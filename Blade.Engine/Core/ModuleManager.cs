﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blade.Engine.Graphics;
using Blade.Engine.Graphics.Effects;

namespace Blade.Engine.Core
{
    public class ModuleManager
	{
		public Dictionary<string, GameModule> Modules { get; protected set; } = new Dictionary<string, GameModule>();
		protected GameModule PreviousModule;
		protected GameModule CurrentModule;
		public BladeGame Game { get; protected set; }

        //Experimental
        TransitionEffect TransitionEffect;
        string NextScreen;
        public bool IsTransitioning = false;
		private float fadeOpacity = 0;
		private float opacityPerSecond;

		public ModuleManager(BladeGame game)
		{
			Game = game;
        }

        public void AddDefaultModule(string name, GameModule module)
        {
            CurrentModule = module;
            AddModule(name, module);
            CurrentModule.OnStart();
            CurrentModule.OnResume(null);
        }

        public void AddModule(string name, GameModule module)
		{
			module.ModuleManager = this;
			Modules.Add(name, module);
            module.OnStart();
        }

        public void ChangeModule(string moduleName, dynamic parameters = null, TransitionEffect transitionEffect = null)
        {
            //Experimental
            opacityPerSecond = 1 / 64f;//255/5;
            IsTransitioning = true;

            PreviousModule = CurrentModule;
            CurrentModule.OnPause();
            if (transitionEffect != null)
            {
                TransitionEffect = transitionEffect;
                this.TransitionEffect.OnTransitionEnded = () =>
                {
                    CurrentModule = Modules[moduleName];
                    CurrentModule.OnResume(parameters);
                };
                NextScreen = moduleName;
            }
            else
            {
                CurrentModule = Modules[moduleName];
                CurrentModule.OnResume(parameters);
            }
        }

        public void RemoveModule(string key)
        {
            CurrentModule.OnPause();
            foreach (KeyValuePair<string, GameScreen> entry in Modules[key].Screens)
            {
                entry.Value.Destroy();
            }
            Modules.Remove(key);
        }

        public void Update(float delta)
        {
            if (TransitionEffect != null)
            {
                TransitionEffect.Update(delta);
                if (TransitionEffect.IsFinished)
                {
                    CurrentModule = Modules[NextScreen];
                    CurrentModule.OnResume();
                    TransitionEffect = null;
                    NextScreen = null;
                }
            }
            else
            {
                CurrentModule.Update(delta);
            }
        }

        //TODO: Add transitions
        public void Draw(SpriteBatch spriteBatch)
        {
            if (TransitionEffect != null)
            {
                TransitionEffect.Draw(spriteBatch);
            }
            else
            {
                CurrentModule.Draw(spriteBatch);
            }
        }
    }
}
