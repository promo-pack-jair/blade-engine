﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Blade.Engine.Core;
using Blade.Engine.Graphics.Effects;

namespace Blade.Engine.Graphics
{
	public abstract class GameModule 
    {
        public Dictionary<string, GameScreen> Screens { get; protected set; } = new Dictionary<string, GameScreen>();
		public GameScreen CurrentScreen { get; protected set; }
        public ModuleManager ModuleManager { get; set; }

        TransitionEffect TransitionEffect;
        string NextScreen;

        public abstract void OnStart();
        public virtual void OnResume(dynamic parameters = null)
        {
            CurrentScreen.OnResume();
        }
        public virtual void OnPause()
        {
            CurrentScreen.OnPause();
        }

        public void AddDefaultScreen(string key, GameScreen screen)
		{
			CurrentScreen = screen;
			AddScreen(key, screen);
            CurrentScreen.OnAttach();
            CurrentScreen.OnResume();
        }

		public void AddScreen(string key, GameScreen screen)
		{
			screen.ContainingModule = this;
			Screens.Add(key, screen);
            screen.OnAttach();
		}

		public void ChangeScreen(string screen, TransitionEffect transitionEffect = null)
		{
            CurrentScreen.OnPause();
            if (transitionEffect != null)
            {
                TransitionEffect = transitionEffect;
                this.TransitionEffect.OnTransitionEnded = () =>
                {
                    CurrentScreen = Screens[screen];
                    CurrentScreen.OnResume();
                };
                NextScreen = screen;
            }
            else
            {
                CurrentScreen = Screens[screen];
                CurrentScreen.OnResume();
            }
        }

        public void RemoveScreen(string key)
        {
            CurrentScreen.OnPause();
            CurrentScreen.Destroy();
            Screens.Remove(key);
        }

        public virtual void Update(float delta)
        {
            if (TransitionEffect != null)
            {
                TransitionEffect.Update(delta);
                if (TransitionEffect.IsFinished)
                {
                    CurrentScreen = Screens[NextScreen];
                    CurrentScreen.OnResume();
                    TransitionEffect = null;
                    NextScreen = null;
                }
            }
            else
            {
                CurrentScreen.Update(delta);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (TransitionEffect != null)
            {
                TransitionEffect.Draw(spriteBatch);
            }
            else
            {
                CurrentScreen.Draw(spriteBatch);
            }
        }

        //Controller Interface
        /*
		void OnButtonPressed(int keyCode);
		void OnButtonReleased(int keyCode);
		void GamePadConnected(int playerIndex);
		void GamePadDisconnected(int playerIndex);
		void PointerPressed(int x, int y);
		void PointerDragged(int x, int y);
		void PointerReleased(int x, int y);
		*/
    }
}
