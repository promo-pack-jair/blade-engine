﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Blade.Engine.ContentImporters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Blade.Engine.Core
{
	public struct ResourceLoadInfo
	{
		public string Name;
		public int Type;

		public ResourceLoadInfo(String name, int type)
		{
			Name = name;
			Type = type;
		}
	}

	public static class ResourceManager
    {
		//Private Fields
        private static ContentManager Content;
        private static string EncryptionKey;
        private static bool IsInitialized;
	    private static readonly Type[] NATIVE_CONTENT_TYPES = {
			typeof(Model),
			typeof(SpriteFont),
		    typeof(Texture2D),
			typeof(Song),
			typeof(SoundEffect)
	    };

        //Fonts
        private static Dictionary<string, SpriteFont> Fonts;

        //Images
        //private static Dictionary<string, Atlas> Atlases;
        private static Dictionary<string, Texture2D> Textures;

        //Audio
        private static Dictionary<string, Song> Songs;
        private static Dictionary<string, SoundEffect> SoundEffects;

		//Experimental
		public static Dictionary<Type, Dictionary<string, object>> CustomAssets;
		private static Dictionary<Type, ICustomContentImporter> CustomContentProcessors;

		public static Texture2D BLANK_SQUARE;

        public static void Initialize(ContentManager content, string encryptionKey,
			GraphicsDevice device)
        {
            if (!IsInitialized)
            {
                Content = content;
				Songs = new Dictionary<string, Song>();
				Fonts = new Dictionary<string, SpriteFont>();
				Textures = new Dictionary<string, Texture2D>();
                SoundEffects = new Dictionary<string, SoundEffect>();
                EncryptionKey = encryptionKey;

				//Experimental
				CustomAssets = new Dictionary<Type, Dictionary<string, object>>();
				CustomContentProcessors = new Dictionary<Type, ICustomContentImporter>();

				//Add Framework Content Processors
				//AddCustomType(new BitmapFontImporter());
				AddCustomType(new TileMapInfoImporter());
				//AddCustomType<Atlas>(new BitmapFontImporter());

				//EmbeddedAssets
				//var assembly = typeof(ResourceManager).Assembly;
				//Stream stream = assembly.GetManifestResourceStream("Framework.EmbeddedResources.blank.png");
				//BLANK_SQUARE = Texture2D.FromStream(device, stream);
				//embedContentManager.Load<Texture2D>("blank");
				BLANK_SQUARE = new Texture2D(device, 1, 1);
				BLANK_SQUARE.SetData(new[] { Color.White });
	            IsInitialized = true;
				//Get<TileMapInfo>("sample");
            }
        }

		public static void AddCustomType<T>(CustomContentImporter<T> importer)
		{
			if (!CustomAssets.ContainsKey(typeof(T)))
			{
				CustomAssets.Add(typeof(T), new Dictionary<string, object>());
				CustomContentProcessors.Add(typeof(T), importer);
			}
			else
			{
				throw new Exception("Resource Manager can't have more than one importer for the same type!");
			}
		}

		//For testing only
		public static T Get<T>(string resourceName)
		{
			if (NATIVE_CONTENT_TYPES.Contains(typeof(T)))
			{
				return Content.Load<T>(resourceName);
			}

			//Experimental
			return LoadCustomAsset<T>(resourceName);
		}

        public static Texture2D LoadTexture(string textureName)
        {
            if (!Textures.ContainsKey(textureName))
            {
                Textures.Add(textureName, Content.Load<Texture2D>(textureName));
            }
            return Textures[textureName];
        }

		public static T LoadCustomAsset<T>(string resourceName)
		{
			if (!CustomAssets[typeof(T)].ContainsKey(resourceName))
			{
				var contentProcessor = (CustomContentImporter<T>)CustomContentProcessors[typeof(T)];
				CustomAssets[typeof(T)].Add(resourceName, contentProcessor.LoadResource(Content.RootDirectory + Path.DirectorySeparatorChar + resourceName));
			}
			return (T)CustomAssets[typeof(T)][resourceName];
		}

		public static Song LoadSong(string songName)
		{
			if (!Songs.ContainsKey(songName))
			{
				Songs.Add(songName, Content.Load<Song>(songName));
			}
			return Songs[songName];
		}

		public static SoundEffect LoadSoundEffect(string sfxName)
		{
			if (!SoundEffects.ContainsKey(sfxName))
			{
				SoundEffects.Add(sfxName, Content.Load<SoundEffect>(sfxName));
			}
			return SoundEffects[sfxName];
		}

        //TODO: Add remaining Methods
    }
}
