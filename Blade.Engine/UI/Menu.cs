﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blade.Engine.Graphics;

namespace Blade.Engine.UI
{
	public enum MenuState
	{
		Active,
		Inactive,
		TransitionOn,
		TransitionOff,
		Hidden
	}

	public class OptionSelectedEventArgs : EventArgs
	{
		public Menu ParentMenu { get; set; }
		public int OptionIndex { get; set; }
		public MenuEntry FocusedOption { get; set; }
		public MenuKey PressedKey { get; set; }

		public OptionSelectedEventArgs(Menu parentMenu, int optionIndex, MenuEntry focusedOption, MenuKey pressedKey)
		{
			ParentMenu = parentMenu;
			OptionIndex = optionIndex;
			FocusedOption = focusedOption;
			PressedKey = pressedKey;
		}
	}

	public enum MenuKey
	{
		Up, Down, Left, Right, Accept, Cancel,
		//Square in PS Controller
		ActionKey1,
		//Triangle in PS Controller
		ActionKey2,
		//L1 in PS Controller
		ScrollLeft,
		//R1 in PS Controller
		ScrollRight,
		//L2 in PS Controller
		ScrollBeginning,
		//R2 in PS Controller
		ScrollEnd,
		//Select in PS Controller
		Info,
		//Ignored Value
		None
	}

	public enum MenuMode
	{
		PositionBased,
		OptionBased
	}

	//TODO: Add Option for menu that requires confirmation to exit
	public class Menu : DisplayObjectContainer
	{
		public static readonly MenuKey[] DEFAULT_NAVIGATION_KEYS = {
			MenuKey.Up, MenuKey.Down, MenuKey.Left, MenuKey.Right, MenuKey.Accept, MenuKey.Cancel,
			MenuKey.ScrollLeft, MenuKey.ScrollRight, MenuKey.ScrollBeginning, MenuKey.ScrollEnd
		};

		public List<MenuEntry> Options { get; protected set; }
		public event EventHandler<OptionSelectedEventArgs> OnAccept;

		public bool IsEnabled { get; set; }
		public bool IsCyclical { get; set; }
		//FIXME
		private int _focusIndex;
		public int FocusIndex {
			get { return _focusIndex; }
			set
			{
				if (value != _focusIndex)
				{
					if (value < 0)
					{
						value = (IsCyclical) ? Options.Count - 1 : 0;
					}
					else if (value >= Options.Count)
					{
						value = (IsCyclical) ? 0 : Options.Count - 1;
					}
					Options[_focusIndex].FocusLost();
					Options[value].FocusGained();
					_focusIndex = value;
				}
			}
		}
		public int Columns { get; set; }

		public readonly MenuMode Mode;
		public string MenuTitle { get; set; }

		public Menu(MenuMode mode, bool isEnabled = true, bool isCyclical = true)
		{
			IsEnabled = isEnabled;
			IsCyclical = isCyclical;
			FocusIndex = 0;
			Columns = 1;

			Options = new List<MenuEntry>();
			Mode = mode;

			//TODO: Add default MenuItems positioning
		}

		public Menu(bool isEnabled = true, bool isCyclical = true)
			: this(MenuMode.OptionBased, isEnabled, isCyclical)
		{
		}

		public virtual void OnMenuKeyPressed(MenuKey key)
		{
			if (DEFAULT_NAVIGATION_KEYS.Contains(key))
			{
				DoMenuNavigation(key);
			}
			else
			{
				ActionMenuKeyPressed(key);
			}
		}

		public void AddOption(MenuEntry entry)
		{
			if (Options.Count == 0 && FocusIndex == 0)
			{
				entry.FocusGained(true);
			}
			Options.Add(entry);
			AddChild(entry);
		}

		public virtual void OnClick(int x, int y)
		{
			for (int i = 0; i < Options.Count; i++)
			{
				if (Options[i].X <= x && x <= (Options[i].X + Options[i].Width) &&
				    Options[i].Y <= y && y <= (Options[i].X + Options[i].Height))
				{
					Options[i].OnClick(x, y);
				}
			}
		}

		protected virtual void DoMenuNavigation(MenuKey key)
		{
			switch (key)
			{
				case MenuKey.Down:
				{
					MoveCursor(Columns);
					break;
				}
				case MenuKey.Up:
				{
					MoveCursor(-Columns);
					break;
				}
				case MenuKey.Left:
				{
					MoveCursor(-1);
					break;
				}
				case MenuKey.Right:
				{
					MoveCursor(1);
					break;
				}
				case MenuKey.Accept:
				{
					if (Mode == MenuMode.PositionBased)
					{
						if (OnAccept != null)
						{
							OnAccept(this, new OptionSelectedEventArgs(this, FocusIndex, Options[FocusIndex], key));
						}
					}
					else
					{
						Options[FocusIndex].OnSelected(new OptionSelectedEventArgs(this, FocusIndex, Options[FocusIndex], key));
					}
					break;
				}
				case MenuKey.Cancel:
				{
					OnCancel();
					break;
				}
				case MenuKey.ScrollLeft:
				{

					break;
				}
				case MenuKey.ScrollRight:
				{

					break;
				}
				case MenuKey.ScrollBeginning:
				{
					FocusIndex = 0;
					break;
				}
				case MenuKey.ScrollEnd:
				{
					FocusIndex = Options.Count - 1;
					break;
				}
			}
		}

		protected virtual void OnCancel() { }

		protected virtual void MoveCursor(int valueToAdd)
		{
			FocusIndex += valueToAdd;
		}

		protected virtual void ActionMenuKeyPressed(MenuKey key) { }
	}
}
