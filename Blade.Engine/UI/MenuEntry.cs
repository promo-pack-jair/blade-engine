﻿using System;
using Blade.Engine.Graphics;

namespace Blade.Engine.UI
{
	public abstract class MenuEntry : DisplayObjectContainer
	{
		/// <summary>
		/// Tracks a fading selection effect on the entry.
		/// </summary>
		/// <remarks>
		/// The entries transition out of the selection effect when they are deselected.
		/// </remarks>
		float selectionFade;

		public bool CanAcceptfocus { get; set; }
		public bool IsFocused { get; set; }
		public event EventHandler<OptionSelectedEventArgs> Selected;

		//For multiselect?
		public bool IsMarked { get; set; }

		protected MenuEntry()
		{
			CanAcceptfocus = true;
		}

		public void OnSelected(OptionSelectedEventArgs e)
		{
			if (Selected != null)
			{
				Selected(this, e);
			}
		}

		public virtual void OnClick(int x, int y) { }
		public virtual void OnLongClick(int x, int y) { }

		public override void Update(float delta)
		{
			base.Update(delta);
			float fadeSpeed = delta * 4;

			if (IsFocused)
				selectionFade = Math.Min(selectionFade + fadeSpeed, 1);
			else
				selectionFade = Math.Max(selectionFade - fadeSpeed, 0);
		}

		public virtual void FocusGained(bool firstTrigger = false)
		{
			IsFocused = true;
		}

		public virtual void FocusLost()
		{
			IsFocused = false;
		}
	}
}
