﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Blade.Engine.Util
{
	///<summary>Provides operations for Tandom Number Generation</summary>
    public static class RNG
    {
        public static readonly Random Random = new Random();

		/*
		/// <summary>
		/// Resets the seed for the RNG.
		/// </summary>
		public static void Reset()
		{
			Random = new Random((int)DateTime.Now.Ticks);
		}
		*/

		public static bool RandomBool()
		{
			return Random.NextDouble() >= 0.5;
		}

        public static int RandomInt()
        {
            return Random.Next();
        }

        public static double RandomDouble()
        {
            return Random.NextDouble();
        }

        public static bool RandomChance(byte percentChance)
        {
            return (Random.NextDouble() * 100) <= percentChance;
        }

        //Returns a number in range including any of the range extremes
        public static int RandomRange(int min, int max)
        {
            return Random.Next(min, max + 1);
        }

		/// <summary>
		/// Return random int from range 0 to max
		/// </summary>
		/// <param name="max">The maximum inclusive value of range.</param>
		/// <returns>Returns a random integer within 0 and max</returns>
        public static int RandomRange(int max)
        {
            return Random.Next(max + 1);
        }

        public static int RandomPositionInCollection(int count)
        {
            return Random.Next(count);
        }

		public static T RandomElementInCollection<T>(ICollection<T> enumerable)
		{
			return enumerable.ElementAt(Random.Next(enumerable.Count));
		}
    }
}
