﻿using System;

namespace Blade.Engine.Util
{
    public class OverInt
    {
        public byte index = 0;
        public byte limit;

        public OverInt(byte limiter)
        {
            limit = limiter;
        }

        //--------------------------------------------------------
        //      Arithmetic Operators
        //--------------------------------------------------------

        public static OverInt operator +(OverInt a, byte b)
        {
            a.Add(b);
            return a;
        }

        public static OverInt operator ++(OverInt a)
        {
            a.Add(1);
            return a;
        }

        public static OverInt operator -(OverInt a, byte b)
        {
            a.Substract(b);
            return a;
        }

        public static OverInt operator --(OverInt a)
        {
            a.Substract(1);
            return a;
        }

        public static OverInt operator *(OverInt a, byte b)
        {
            a.Multiply(b);
            return a;
        }

        public static OverInt operator /(OverInt a, byte b)
        {
            a.Divide(b);
            return a;
        }

        //--------------------------------------------------------
        //      Implicit Conversions
        //--------------------------------------------------------

        // User-defined conversion from OverInt to int
        public static implicit operator int(OverInt c)
        {
            return (int)(c.index);
        }

        // User-defined conversion from int to OverInt
        public static implicit operator OverInt(int d)
        {
            return new OverInt((byte)d).index = (byte)d;
        }

        //Overrides ToString() Method to return the value of index
        public override string ToString()
        {
            return (String.Format("{0}", index.ToString()));
        }

        //--------------------------------------------------------
        //      Operator Methods
        //--------------------------------------------------------

        private byte Add(byte value)
        {
            if (index != limit && index + value > limit)
                index = limit;
            else if (index == limit)
                index = 0;
            else
                index += value;
            return index;
        }

        private byte Substract(byte value)
        {
            if (index != 0 && index - value < 0)
                index = 0;
            else if (index == 0)
                index = limit;
            else
                index -= value;
            return index;
        }

        private byte Multiply(byte value)
        {
            index *= value;
            if (index == limit)
                return index;
            return Add(0);
        }

        private byte Divide(byte value)
        {
            index /= value;
            return (byte)(index / value);
        }
    }
}
