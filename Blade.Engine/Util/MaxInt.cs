﻿namespace Blade.Engine.Util
{
    public class MaxInt
    {
        public int Max { get; protected set; }
        private int _val;
        public int Value
        {
            get
            {
                return _val;
            }
            set
            {
                if (value > Max)
                    _val = Max;
                else
                    _val = value;
            }
        }

        public MaxInt(int value, int limit)
        {
            this.Max = limit;
            this._val = value;
        }

        public static implicit operator int(MaxInt c)
        {
            return c._val;
        }

        // User-defined conversion from int to OverInt
        public static implicit operator MaxInt(int d)
        {
            return new MaxInt(d, d);
        }

        public override string ToString()
        {
            return _val.ToString();
        }
    }
}
