﻿using Microsoft.Xna.Framework.Storage;
using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization.Formatters.Binary;

namespace Blade.Engine.Util
{
	[Serializable]
    public abstract class SaveFile
	{
		private string SaveName;

		public SaveFile() { }

	    public SaveFile(string saveName)
	    {
		    LoadData(saveName);
		    SaveName = saveName;
	    }

		//FIXME: Optimize
        public virtual void SaveData(string saveName)
        {
			var store = IsolatedStorageFile.GetUserStoreForApplication();

			if (store.FileExists(saveName))
			{
				var fs = store.OpenFile(saveName, FileMode.Open);
				using (var stream = new BinaryReader(fs))
				{
					var formatter = new BinaryFormatter();

					var saveFile = (SaveFile)formatter.Deserialize(stream.BaseStream);
				}
			}
			else
			{
				var fs = store.CreateFile(saveName);
				using (var sw = new BinaryWriter(fs))
				{
					var formatter = new BinaryFormatter();
					//Initialize to Default Values
					using (var stream = new MemoryStream())
					{
						formatter.Serialize(stream, this);
						stream.Seek(0, SeekOrigin.Begin);
						sw.Write(stream.ToArray());
					}
				}
			}
        }

        //https://github.com/CartBlanche/MonoGame-Samples/blob/master/Storage/SaveGame.cs
        //https://msdn.microsoft.com/en-us/library/bb199092.aspx
        public virtual void LoadData(string saveName)
        {
            StorageDevice device = null;// = StorageDevice.ShowStorageDeviceGuide();

            IAsyncResult result = device.BeginOpenContainer("StorageDemo", null, null);

            // Wait for the WaitHandle to become signaled.
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = device.EndOpenContainer(result);

            // Close the wait handle.
            result.AsyncWaitHandle.Close();

            // Add the container path to our file name.
            string filename = "demobinary.sav";

            Stream file = container.OpenFile(filename, FileMode.Open);
            file.Close();

            // Dispose the container.
            container.Dispose();

            var store = IsolatedStorageFile.GetUserStoreForApplication();
            if (store.FileExists(saveName))
            {
                var fs = store.OpenFile(saveName, FileMode.Open);
                using (var reader = new BinaryReader(fs))
                {

                }
            }
        }

        public virtual void DeleteData(StorageDevice device)
        {
            IAsyncResult result = device.BeginOpenContainer("StorageDemo", null, null);

            // Wait for the WaitHandle to become signaled.
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = device.EndOpenContainer(result);

            // Close the wait handle.
            result.AsyncWaitHandle.Close();

            // Add the container path to our file name.
            string filename = "demobinary.sav";

            if (container.FileExists(filename))
            {
                container.DeleteFile(filename);
            }

            // Dispose the container, to commit the change.
            container.Dispose();
        }

		//public static SaveFile CreateSave(string saveName)
		//{
		//	return new SaveFile(saveName);
		//}
    }
}
