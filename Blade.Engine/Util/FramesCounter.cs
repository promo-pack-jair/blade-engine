﻿using Blade.Engine.Graphics;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Util
{
    public class FramesCounter : Text
    {
		public const float SECOND = 1000;
		int frameRate = 0;
		int frameCounter = 0;
		private float elapsedTime = 0;

		public FramesCounter(SpriteFont font) : base(font, "") { }

		public override void Update(float delta)
		{
			elapsedTime += delta;

			if (elapsedTime > SECOND)
			{
				elapsedTime -= SECOND;
				frameRate = frameCounter;
				frameCounter = 0;
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			frameCounter++;

			TextValue = $"FPS:{frameRate}";

			if (Visible)
			{
				base.Draw(spriteBatch);
			}
		}
	}
}
