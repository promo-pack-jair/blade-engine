﻿using Blade.Engine.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Tiles
{
	//TODO: Support animated Tiles
    public class Tile : Sprite, IMapEntity
    {
		public int MapX { get; set; }
		public int MapY { get; set; }
		public bool IsSolid { get; set; }
		public TileMapEvent Event { get; set; }

        public Tile(Texture2D texture, int mapX, int mapY, Rectangle textureRectangle, bool isSolid = false) : base(texture)
        {
            MapX = mapX;
            MapY = mapY;
            IsSolid = isSolid;
            Texture = texture;
	        TexturePosition = textureRectangle;
        }

	    public override void Draw(SpriteBatch spriteBatch)
	    {
			//TODO: Standarize, each Drawable should be responsible of checking only it's own visibility
		    if (Visible)
		    {
			    spriteBatch.Draw(Texture, Position, TexturePosition, Color * Alpha, Rotation, Parent.Position, Scale, SpriteEffects, 0f);
				//TODO: Remove
			    for (int i = 0; i < Childs.Count; i++)
				{
				    Childs[i].Draw(spriteBatch);
			    }
		    }
	    }
    }
}
