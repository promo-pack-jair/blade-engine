﻿using System;
using Microsoft.Xna.Framework;

namespace Blade.Engine.Tiles
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

	public static class DirectionExtension
	{
		public static void AddDirection(this Vector2 vector, Direction direction, float valueToAdd)
		{
			switch (direction)
			{
				case Direction.Up: vector.Y -= valueToAdd;
					break;
				case Direction.Left: vector.X -= valueToAdd;
					break;
				case Direction.Right: vector.X += valueToAdd;
					break;
				case Direction.Down: vector.Y += valueToAdd;
					break;
			}
		}

		public static Direction GetOpossiteDirection(this Direction direction)
		{
			switch (direction)
			{
				case Direction.Up:
                    return Direction.Down;
				case Direction.Left:
                    return Direction.Right;
				case Direction.Right:
                    return Direction.Left;
				case Direction.Down:
					return Direction.Up;
			}
			return Direction.Up;
		}

		public static void AddToVector(this Direction direction, ref Vector2 vector, float valueToAdd)
		{
			switch (direction)
			{
				case Direction.Up: vector.Y -= valueToAdd;
					break;
				case Direction.Left: vector.X -= valueToAdd;
					break;
				case Direction.Right: vector.X += valueToAdd;
					break;
				case Direction.Down: vector.Y += valueToAdd;
					break;
			}
		}

		public static Single GetRelevantDimension(this Vector2 vector, Direction direction)
		{
			if (direction == Direction.Up || direction == Direction.Down)
			{
				return vector.Y;
			}
			return vector.X;
		}

		public static void NormalizeVectorDirection(this Direction direction, Vector2 vector, int factor)
		{
			int nearestMultiple = (int)Math.Round(GetRelevantDimension(vector, direction) / factor, MidpointRounding.AwayFromZero) * factor;
			if (direction == Direction.Up || direction == Direction.Down)
			{
				vector.Y = nearestMultiple;
			}
			else
			{
				vector.X = nearestMultiple;
			}
		}

		public static void NormalizeDirection(this Vector2 vector, Direction direction, int factor)
		{
			int nearestMultiple = (int)Math.Round(GetRelevantDimension(vector, direction) / factor, MidpointRounding.AwayFromZero) * factor;
			if (direction == Direction.Up || direction == Direction.Down)
			{
				vector.Y = nearestMultiple;
			}
			else
			{
				vector.X = nearestMultiple;
			}
		}
	}
}
