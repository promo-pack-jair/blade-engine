﻿using System.Diagnostics;
using Blade.Engine.Graphics;
using Microsoft.Xna.Framework.Graphics;
using Blade.Engine.Core;

namespace Blade.Engine.Tiles
{
    public interface IMapEvent
    {
        void EventStart();
    }

	public abstract class TileMapEvent
	{
		//TODO: Add reference to map
		//TODO: Make drawable
		//TODO: Add reference to game
		public DisplayObjectContainer ParentMap { get; set; }
		public Tile ContainingTile { get; set; }
		public EventTrigger Trigger { get; set; }
		public abstract bool IsAvailabe { get; }

		protected TileMapEvent(EventTrigger trigger)
		{
			Trigger = trigger;
		}

		public abstract void ExecuteEvent();
	}

	public class SampleEvent : TileMapEvent
	{
		public SampleEvent()
			: base(EventTrigger.OnTouch)
		{
		}

		private bool _isAvailable = true;
		public override bool IsAvailabe
		{
			get { return _isAvailable; }
		}

		public override void ExecuteEvent()
		{
			Debug.WriteLine("Event Started");
			AnimatedSprite sprite = new AnimatedSprite();
			sprite.AddDefaultAnimation("Default", new Animation(ResourceManager.Get<Texture2D>("Textures/teleporter_01"), 8, 10, 8, 2, true));
			sprite.CurrentAnimation.Play();
			ContainingTile.AddChild(sprite);
			//BladeGame.Instance.ModuleManager.ChangeModule("test");
			_isAvailable = false;
		}
	}

	public enum EventTrigger
	{
		OnInteraction,
		OnTouch,
		OnMovementStarted
	}
}
