﻿using Blade.Engine.Core;
using Blade.Engine.Graphics;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blade.Engine.Tiles
{
    public class Player : AnimatedSprite, IMapEntity
	{
		public int MapX { get; set; }
		public int MapY { get; set; }
		public bool IsSolid { get; set; }

		private bool IsMoving;
		private int MovementTimer;
		private int MovementDuration;
		public Direction FacingDirection { get; set; }
        public Action OnPostMove { get; set; }

        //TODO: Global
        private TileMap<Tile> Map;
		private int MAP_DIMENSION;

		public Player(object map, int mapDimension = 64)
		{
			MAP_DIMENSION = mapDimension;
			Map = (TileMap<Tile>)map;
			//Add Animations by name
			AddDefaultAnimation(Direction.Down.ToString(),
				new Animation(ResourceManager.Get<Texture2D>("Textures/sprite"), 4, 15, 4), false);
			AddAnimation(Direction.Up.ToString(),
				new Animation(ResourceManager.Get<Texture2D>("Textures/sprite"), 4, 15, 4, 3));
			AddAnimation(Direction.Left.ToString(),
				new Animation(ResourceManager.Get<Texture2D>("Textures/sprite"), 4, 15, 4, 1));
			AddAnimation(Direction.Right.ToString(),
				new Animation(ResourceManager.Get<Texture2D>("Textures/sprite"), 4, 15, 4, 2));
		}

		public override void Update(float delta)
		{
			base.Update(delta);
			if (IsMoving && MovementTimer != 0)
			{
				FacingDirection.AddToVector(ref Position, (MAP_DIMENSION / (float)MovementDuration) * 2);
				FacingDirection.AddToVector(ref Map.Position, MAP_DIMENSION / (float)MovementDuration);
				MovementTimer--;

				//Normalizes the position to integer
				if (MovementTimer == 0)
				{
					FacingDirection.NormalizeVectorDirection(Position, MAP_DIMENSION);
					IsMoving = false;
					switch (FacingDirection)
					{
						case Direction.Up: MapY--;
							break;
						case Direction.Left: MapX--;
							break;
						case Direction.Right: MapX++;
							break;
						case Direction.Down: MapY++;
							break;
					}
					var tileEvent = Map.Background[MapX + (Map.Columns * MapY)].Event;
					if (tileEvent != null && tileEvent.IsAvailabe && tileEvent.Trigger == EventTrigger.OnTouch)
					{
						tileEvent.ExecuteEvent();
					}
                    if (OnPostMove != null)
                    {
                        OnPostMove();
                    }
				}
			}
		}

		public void Interact()
		{
			var nextTile = Map.NextTile(this, FacingDirection);
			if (nextTile != null)
			{
				if (nextTile.Event != null && nextTile.Event.IsAvailabe &&
					nextTile.Event.Trigger == EventTrigger.OnInteraction)
				{
					nextTile.Event.ExecuteEvent();
				}
			}
		}

		public void Move(Direction direction, int movementDuration)
		{
			if (!IsMoving && MovementTimer == 0)
			{
				FacingDirection = direction;
				CurrentAnimation.Reset();
				ChangeCurrentAnimation(direction.ToString());
				var nextTile = Map.NextTile(this, FacingDirection);
				if (nextTile != null)
				{
					if (nextTile.Event != null && nextTile.Event.IsAvailabe &&
						nextTile.Event.Trigger == EventTrigger.OnMovementStarted)
					{
						nextTile.Event.ExecuteEvent();
					}
					else if (!nextTile.IsSolid)
					{
						MovementTimer = movementDuration;
						MovementDuration = movementDuration;
						IsMoving = true;
					}
				}
			}
		}
	}
}
