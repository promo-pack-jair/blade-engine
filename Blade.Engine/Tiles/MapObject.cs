﻿using Blade.Engine.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine.Tiles
{
    public abstract class MapObject
    {
        public int MapX;
        public int MapY;
        public bool IsSolid;
        public bool IsVisible;

        protected MapObject(int mapX = 0, int mapY = 0, bool isSolid = true)
        {
            MapX = mapX;
            MapY = mapY;
            IsSolid = isSolid;
        }

        public virtual void Update(float delta) { }
    }

	public interface IMapEntity
	{
		int MapX { get; set; }
		int MapY { get; set; }
		bool IsSolid { get; set; }
		bool Visible { get; set; }

		void Update(float delta);
		void Draw(SpriteBatch spriteBatch);
	}

	public class SimpleTile : TiledSprite, IMapEntity
	{
		public SimpleTile(Texture2D texture, float widht, float height) : base(texture, widht, height)
		{
		}

		public SimpleTile(Texture2D texture, float width, float height, Rectangle src) : base(texture, width, height, src)
		{
		}

		public int MapX { get; set; }

		public int MapY { get; set;}

		public bool IsSolid { get; set; }
	}
}
