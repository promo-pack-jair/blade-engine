﻿using System.Collections.Generic;
using Blade.Engine.ContentImporters;
using Blade.Engine.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blade.Engine.Core;

namespace Blade.Engine.Tiles
{
	public abstract class TileMap : GameScreen
	{
		//Static members
		public static int TILE_DIMENSION { get; private set; }

		public static void Initialize(int tileDimension)
		{
			TILE_DIMENSION = tileDimension;
		}

		//Class members

		//TileMap Information
		public int Columns { get; protected set; }
		public int Rows { get; protected set; }

		//List of entities

		public abstract Tile GetTileAt(int pos);

		public Tile GetTileAt(IMapEntity entity)
		{
			return GetTileAt(entity.MapX + (Columns * entity.MapY));
		}

		public abstract Tile NextTile(IMapEntity entity, Direction direction);
	}
	//TODO: Add support for static background image, like Mario
	//TODO: Stretch Map viewport
    public abstract class TileMap<T> : GameScreen where T :Tile
    {
        public int MAP_DIMENSION = 64;
        public int FRAMES_PER_SECOND = 30;
		public int VIEWPORT_HORIZONTAL_TILES = 15;
		public int VIEWPORT_VERTICAL_TILES = 9;

		//TileMap Information
		public int Columns { get; protected set; }
		public int Rows { get; protected set; }

        public List<MapObject> MapObjects { get; set; }

		//Map Objects
	    public T[] Background;
	    private Tile[] Foreground;
		public Player Player { get; protected set; }
	    private Tile[] Entities;

		public TileMapCamera<T> Camera { get; set; }

        //Represents the upper and left MapCoordinates of the camera
        public Vector2 CameraPosition { get; set; }
        private int CameraMovementTimer { get; set; }
        private Direction CameraDirection { get; set; }

		protected TileMap(string resourceName)
			: this(ResourceManager.Get<TileMapInfo>(resourceName))
        {
        }

	    protected TileMap(TileMapInfo mapInfo)
	    {
		    Columns = mapInfo.MapWidth;
		    Rows = mapInfo.MapHeigth;
			Background = new T[mapInfo.Tiles.Length];
			Foreground = new Tile[mapInfo.Tiles.Length];
			MapObjects = new List<MapObject>();
		    Camera = new TileMapCamera<T>
		    {
			    Map = this,
			    Width = MAP_DIMENSION * VIEWPORT_HORIZONTAL_TILES,
			    Height = MAP_DIMENSION * VIEWPORT_VERTICAL_TILES
		    };

		    Player = new Player(this, MAP_DIMENSION);

		    int tileColumn = 0;
			int tileRow = 0;
			for (int i = 0; i < mapInfo.Tiles.Length; i++)
			{
				T tile = GetTileFromMapInfo(mapInfo.Tiles[i]);
				tile.MapX = tileColumn;
				tile.MapY = tileRow;
				tile.X = MAP_DIMENSION * tile.MapX;
				tile.Y = MAP_DIMENSION * tile.MapY;
				//TODO: Remove
				tile.AddChild(new Text(ResourceManager.Get<SpriteFont>("Fonts/SampleFont"), i.ToString()));
				tile.GetChildAt(0).X = MAP_DIMENSION * tile.MapX;
				tile.GetChildAt(0).Y = MAP_DIMENSION * tile.MapY;
				if (i == 1)
				{
					tile.Event = new SampleEvent();
					tile.Event.ParentMap = this;
					tile.Event.ContainingTile = tile;
				}
				Background[i] = tile;
				AddChild(tile);

				tileColumn++;
				if (tileColumn == Columns)
				{
					tileColumn = 0;
					tileRow++;
				}
            }
            AddChild(Player);
        }

	    protected abstract T GetTileFromMapInfo(object tileInfo);
		
        public override void Update(float delta)
        {
            if (CameraMovementTimer != 0)
            {
				CameraPosition.AddDirection(CameraDirection, MAP_DIMENSION / FRAMES_PER_SECOND);
				CameraMovementTimer--;

				//Normalizes the position to integer
				if (CameraMovementTimer == 0)
				{
					CameraPosition.NormalizeDirection(CameraDirection, MAP_DIMENSION);
				}
            }
			Player.Update(delta);
            for (int i = 0; i < MapObjects.Count; i++)
            {
                MapObjects[i].Update(delta);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
	        var inBound = Camera.InBound();
			foreach (var tile in inBound)
	        {
                tile.Draw(spriteBatch);
	        }
			Player.Draw(spriteBatch);
            for (int i = 0; i < MapObjects.Count; i++)
            {
                if (MapObjects[i].IsVisible)
                {
                    //MapObjects[i].Draw();
                }
            }
        }

	    public T NextTile(IMapEntity entity, Direction direction)
	    {
			int index = entity.MapX + (Columns * entity.MapY);
			switch (direction)
			{
				case Direction.Up:
					index -= Columns;
					break;
				case Direction.Left:
					if (index % Columns == 0)
					{
						index = -1;
					}
					index--;
					break;
				case Direction.Right:
					index++;
					if (index % Columns == 0)
					{
						index = -1;
					}
					break;
				case Direction.Down:
					index += Columns;
					break;
			}
		    return (index >= 0 && index < Background.Length) ? Background[index] : null;
	    }

        public void MoveCamera(Direction direction)
        {
			if (CameraMovementTimer == 0)
			{
				CameraDirection = direction;
				CameraMovementTimer = FRAMES_PER_SECOND;
			}
			//Else camera buffer?
        }
    }
}
