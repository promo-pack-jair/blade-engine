﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Blade.Engine.Tiles
{
	public class TileMapCamera<T> where T : Tile
	{
		//TODO: Implement
		public enum CameraMode
		{
			CenterPlayer,
			Static,
			VerticalScroll,
			HorizontalScroll,
			FullScroll
		}
		public TileMap<T> Map { get; set; }
		public int MapX { get; set; }
		public int MapY { get; set; }
		public float X { get; set; }
		public float Y { get; set; }
		private int _width;
		public int Width
		{
			get { return _width; }
			set
			{
				Bound.Width = value;
				_width = value;
			}
		}

		private int _height;
		public int Height
		{
			get { return _height; }
			set
			{
				Bound.Height = value;
				_height = value;
			}
		}

		private Rectangle Bound;

		public Tile Player;

		//Represents tile index of the starting tile of the camera
		private int TileIndex;
		private int EndIndex = 71;
		public int CameraColumns = 12;

		//Movement
		private bool IsMoving;
		private int MovementTimer;
		private int MovementDuration;
		public Direction MovementDirection { get; set; }

		public IEnumerable<T> InBound()
		{
			//Try to add 1 extra tile at the border for camera movement
			int start = MapX + (Map.Columns * MapY);
			start -= (Map.Columns + 1);
			int end = (MapX * MapY);
			//Find the index of the last Tile of the map
			EndIndex = MapX + (Map.Columns * MapY) + (Map.Columns * (Map.VIEWPORT_VERTICAL_TILES + 1));
			//Add Border to the last Tile
			EndIndex += Map.VIEWPORT_HORIZONTAL_TILES + 1;
			EndIndex = Math.Min(EndIndex, Map.Background.Length);
			int rows = 0;
			for (int i = Math.Max(start, 0); i <= EndIndex; i++)
			{
				//TODO: Optimize
				if (Map.Background[i].MapX >= this.MapX - 1 && Map.Background[i].MapX <= this.MapX + Map.VIEWPORT_HORIZONTAL_TILES + 1&&
					Map.Background[i].MapY >= this.MapY - 1 && Map.Background[i].MapY <= this.MapY + Map.VIEWPORT_VERTICAL_TILES + 1)
				{
					yield return Map.Background[i];
				}
			}
		}

		public void Move(Direction direction, int movementDuration)
		{
			//if (!IsMoving && MovementTimer == 0)
			//{
			//	MovementDirection = direction;
			//	var nextTile = Map.NextTile(this, MovementDirection);
			//	if (nextTile != null && !nextTile.IsSolid)
			//	{
			//		MovementTimer = movementDuration;
			//		MovementDuration = movementDuration;
			//		IsMoving = true;
			//	}
			//}
		}
	}
}
