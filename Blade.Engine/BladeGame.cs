﻿using System;
using System.Collections.Generic;
using Blade.Engine.Core;
using Blade.Engine.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blade.Engine
{
    public abstract class BladeGame : Game, ITimerManager
	{
		public static Color ClearColor = Color.Black;

		protected GraphicsDeviceManager Graphics;
		protected SpriteBatch SpriteBatch;

		private Matrix ScaleMatrix = Matrix.CreateScale(1, 1, 1);
		public Point ScreenSize { get; protected set; }

		//Framework
		public ModuleManager ModuleManager { get; protected set; }

		//Timer
		protected List<TimerEventSubscriptor> TimerEventSubscriptors = new List<TimerEventSubscriptor>();
		protected List<TimerEventSubscriptor> TimerEventCleanUp = new List<TimerEventSubscriptor>();
#if DEBUG
        private Text debugText;
#endif

        protected BladeGame(int screenWidth, int screenHeight, int fps)
		{
			Graphics = new GraphicsDeviceManager(this);
			ModuleManager = new ModuleManager(this);
			TargetElapsedTime = TimeSpan.FromSeconds(1f / fps);

			//Tell the framework to call the Update() method every time before the call to the Draw() method instead at regular intervals
			//IsFixedTimeStep = false;

			//Call the Draw() method at maximum intensity
			//Graphics.SynchronizeWithVerticalRetrace = false;

			Graphics.PreferredBackBufferWidth = screenWidth;
			Graphics.PreferredBackBufferHeight = screenHeight;
			ScreenSize = new Point(screenWidth, screenHeight);

            //Experimental
            Instance = this;
		}

		protected override void Initialize()
		{
			base.Initialize();
			SpriteBatch = new SpriteBatch(GraphicsDevice);

			//Graphics.GraphicsDevice.BlendState = BlendState.Opaque;

			//FIXME: Used to scale all content, needs fixing
			//FIXME: Use GraphicsDevice.Viewport.Width&Height instead of defaultAdapter to get current window size
			//var displayMode = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
			ScaleMatrix = Matrix.CreateScale(GraphicsDevice.Viewport.Width / (float)ScreenSize.X,
				GraphicsDevice.Viewport.Height / (float)ScreenSize.Y, 1);
			//Following line should be used to correct mouse input to scale, scale mouse position by next vector
			//var ScaleVector2 = new Vector2(scaleMatrix.M11, scaleMatrix.M22);
			ResourceManager.Initialize(Content, null, GraphicsDevice);
#if DEBUG
            debugText = new Text(ResourceManager.Get<SpriteFont>("Fonts/SampleFont"), "", 820);
            debugText.Y = 360;
            debugText.Color = Color.Yellow;
            debugText.ScaleX = debugText.ScaleY = 0.70f;
#endif
        }

        protected override void Update(GameTime gameTime)
		{
			float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

			GameInput.Update();
			ModuleManager.Update(deltaTime);

			//for (var i = 0; i < TimerEventCleanUp.Count; i++)
			//{
			//	TimerEventSubscriptors.Remove(TimerEventCleanUp[i]);
			//	TimerEventCleanUp.Remove(TimerEventCleanUp[i]);
			//}

			for (var i = 0; i < TimerEventSubscriptors.Count; i++)
			{
				TimerEventSubscriptors[i].Update(deltaTime);
				if (TimerEventSubscriptors[i].IsFinished)
				{
					TimerEventSubscriptors.RemoveAt(i);
					i--;
				}
			}
		}

		protected override void Draw(GameTime gameTime)
		{
			base.Draw(gameTime);

			Graphics.GraphicsDevice.Clear(ClearColor);

			SpriteBatch.Begin(transformMatrix: ScaleMatrix);
			ModuleManager.Draw(SpriteBatch);
#if DEBUG
            debugText.Draw(SpriteBatch);
#endif
            SpriteBatch.End();
		}

		public void AddListener(TimerEventSubscriptor listener)
		{
			listener.TimerManager = this;
			TimerEventSubscriptors.Add(listener);
		}

		public void RemoveListener(TimerEventSubscriptor listener)
		{
			TimerEventCleanUp.Add(listener);
		}

#if DEBUG
        public void LogMessage(string message)
        {
            if (debugText.TextValue.Split('\n').Length > 4)
            {
                debugText.SetText(debugText.TextValue.Remove(0, debugText.TextValue.IndexOf('\n') + 1) + message + "\n");
            }
            else
            {
                debugText.SetText(debugText.TextValue + message + "\n");
            }
        }
#endif

        //Experimental
        public static BladeGame Instance;
	}
}
